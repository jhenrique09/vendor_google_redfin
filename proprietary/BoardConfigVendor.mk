# Generated by adevtool; do not edit
# For more info, see https://github.com/kdrag0n/adevtool

BUILD_BROKEN_ELF_PREBUILT_PRODUCT_COPY_FILES := true

BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4

AB_OTA_PARTITIONS += \
    vendor

TARGET_BOARD_INFO_FILE := vendor/google/redfin/firmware/android-info.txt

BOARD_VENDOR_SEPOLICY_DIRS += \
    hardware/google/pixel-sepolicy/confirmationui_hal \
    hardware/google/pixel-sepolicy/input \
    hardware/google/pixel-sepolicy/connectivity_thermal_power_manager \
    vendor/google/redfin/sepolicy

# Missing vendor SELinux context: /vendor/bin/hw/vendor\.google\.exo_camera_injection@1\.1-service u:object_r:hal_exo_camera_injection_exec:s0
# Missing vendor SELinux context: vendor.google.exo_camera_injection::IExoCameraInjection u:object_r:hal_exo_camera_injection_hwservice:s0
# Missing vendor SELinux context: user=_app seinfo=platform name=com.google.pixel.exo domain=exo_app type=app_data_file levelFrom=all
